﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("01/01/0001 : Expected Result? " + (DateTime.Parse("01/01/0001").ToString() == "1/1/0001 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/0001 : Test Failed");
            }
            try
            {
                Console.WriteLine("1/01/0001 : Expected Result? " + (DateTime.Parse("1/01/0001").ToString() == "1/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("1/01/0001 : Test Passed");
            }
            try
            {
                Console.WriteLine("01/1/0001 : Expected Result? " + (DateTime.Parse("01/1/0001").ToString() == "1/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/0001 : Test Passed");
            }
            try
            {
                Console.WriteLine("01/01/1 : Expected Result? " + (DateTime.Parse("01/01/1").ToString() == "1/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/1 : Test Passed");
            } try
            {
                Console.WriteLine("12/01/0001 : Expected Result? " + (DateTime.Parse("12/01/0001").ToString() == "12/1/0001 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("12/01/0001 : Test Failed");
            } try
            {
                Console.WriteLine("01/31/0001 : Expected Result? " + (DateTime.Parse("01/31/0001").ToString() == "1/31/0001 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/31/0001 : Test Failed");
            } try
            {
                Console.WriteLine("01/01/9999 : Expected Result? " + (DateTime.Parse("01/01/9999").ToString() == "1/1/9999 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/9999 : Test Failed");
            } try
            {
                Console.WriteLine("00/01/0001 : Expected Result? " + (DateTime.Parse("00/01/0001").ToString() == "0/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("00/01/0001 : Test Passed");
            } try
            {
                Console.WriteLine("01/00/0001 : Expected Result? " + (DateTime.Parse("01/00/0001").ToString() == "1/0/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/00/0001 : Test Passed");
            } try
            {
                Console.WriteLine("01/01/0000 : Expected Result? " + (DateTime.Parse("01/01/0000").ToString() == "1/1/0000 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/0000 : Test Passed");
            } try
            {
                Console.WriteLine("13/01/0001 : Expected Result? " + (DateTime.Parse("13/01/0001").ToString() == "13/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("13/01/0001 : Test Passed");
            } try
            {
                Console.WriteLine("01/32/0001 : Expected Result? " + (DateTime.Parse("01/32/0001").ToString() == "1/32/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/32/0001 : Test Passed");
            } try
            {
                Console.WriteLine("02/29/0001 : Expected Result? " + (DateTime.Parse("02/29/0001").ToString() == "2/29/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("02/29/0001 : Test Passed");
            } try
            {
                Console.WriteLine("04/31/0001 : Expected Result? " + (DateTime.Parse("04/31/0001").ToString() == "4/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("04/31/0001 : Test Passed");
            } try
            {
                Console.WriteLine("06/31/0001 : Expected Result? " + (DateTime.Parse("06/31/0001").ToString() == "6/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("06/31/0001 : Test Passed");
            } try
            {
                Console.WriteLine("09/31/0001 : Expected Result? " + (DateTime.Parse("09/31/0001").ToString() == "9/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("09/31/0001 : Test Passed");
            } try
            {
                Console.WriteLine("11/31/0001 : Expected Result? " + (DateTime.Parse("11/31/0001").ToString() == "11/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("11/31/0001 : Test Passed");
            }
        }
    }
}
